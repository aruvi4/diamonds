from models.Deck import Card
import numpy as np

PAD = 10
PRIZE_CARD_W = 50
PRIZE_CARD_H = 100
PLAYER_CARD_W = 30
PLAYER_CARD_H = 60
PLAYER_HAND_H = PAD + PLAYER_CARD_H + PAD
PLAYER_HAND_W = PAD + (PLAYER_CARD_W + PAD) * 13
REMAINING_DECK_W = PAD + PRIZE_CARD_W + PAD
REMAINING_DECK_H = PAD + PRIZE_CARD_H + PAD


def make_grid(left, top, width, height, rows, cols):
    each_width = width / cols
    each_height = height / rows
    return [[(l, t, each_width, each_height) 
             for l in np.linspace(left, left + width - each_width, cols)]
             for t in np.linspace(top, top + height - each_height, rows)]

#class PrizeCard:
    
#    def __init__(self, card: Card):
