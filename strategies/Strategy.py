from abc import ABC, abstractmethod
from models.Deck import Card
import random

class Strategy(ABC):

    @abstractmethod
    def pick_card(self, hand: list[Card], top_card: Card) -> Card:    
        raise NotImplementedError()
    
class RandomPicker(Strategy):

    def pick_card(self, hand: list[Card], top_card: Card) -> Card:
        return random.choice(hand)
    
class UserInputPicker(Strategy):

    def pick_card(self, hand: list[Card], top_card: Card) -> Card:
        print(f"your hand is: {list(enumerate(hand))}")
        idx = int(input("pick the card to play"))
        return hand[idx]
    
class ValueMatcher(Strategy):
    def pick_card(self, hand: list[Card], top_card: Card) -> Card:
        return next((card for card in hand if card.val() == top_card.val()), random.choice(hand))

class ThisWontWork(Strategy):

    def random_method1(self):
        print("hello")