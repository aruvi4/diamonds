from game.Game import Game, Player
from models.Deck import Deck
from strategies.Strategy import RandomPicker, UserInputPicker, ValueMatcher, ThisWontWork
from view.View import make_grid

#print(make_grid(0, 0, 100, 100, 10, 10))

p1 = Player("player 1", [_ for _ in Deck().cards if _.suit == 'S'], RandomPicker())
p2 = Player("player 2", [_ for _ in Deck().cards if _.suit == 'H'], RandomPicker())

g = Game([p1, p2])
while not g.is_over():
    g.play_round()
    print(g)
