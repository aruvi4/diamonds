class Card:

    RANKS = 'A23456789TJQK' 
    SUITS = 'SHDC'

    '''
    Initialize with a str containing RANK (A-K-Q-J-T-9-...-2) 
    followed by SUIT (S-H-D-C), like 2C for the 2 of clubs.
    '''
    def __init__(self, notation: str) -> None:
        self.rank, self.suit = list(notation.upper())

    def val(self) -> int:
        #TODO: consider generalizing for situations where Aces are high value
        return self.RANKS.index(self.rank) + 1
    
    def __repr__(self) -> str:
        return f'{self.rank}{self.suit}'

class Deck:

    def __init__(self) -> None:
        self.cards = [Card(f'{rank}{suit}') for rank in Card.RANKS for suit in Card.SUITS]

