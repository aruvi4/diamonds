from models.Deck import Card, Deck
from strategies.Strategy import Strategy
import random

class Player:

    def __init__(self, name, cards: list[Card], strategy: Strategy) -> None:
        self.name = name
        self.cards = cards
        self.suit = self.cards[0].suit
        self.points = 0
        self.strategy = strategy
    
    def play(self, top_card: Card) -> Card:
        print(f"Picking a card for {self.name}")
        self.picked_card = self.strategy.pick_card(self.cards, top_card)
        #self.cards = [_ for _ in self.cards if _ != self.picked_card]
        self.cards.remove(self.picked_card)
        return self.picked_card
    
    def __repr__(self):
        return f"{self.name} using Strategy: {type(self.strategy)}"

class Game:

    def __init__(self, players: list[Player]):
        self.players = players
        self.deck = Deck()
        self.diamonds = [card for card in self.deck.cards if card.suit == 'D']
        random.shuffle(self.diamonds)

    def play_round(self):
        if len(self.diamonds) == 0:
            raise ValueError("No more diamonds to draw!")
        self.top_card = self.diamonds.pop()
        print(f"The top card is {self.top_card}")
        self.played_cards = {player: player.play(self.top_card) for player in self.players}
        winning_rank = max([card.val() for card in self.played_cards.values()])
        #winning_rank = max(self.played_cards.values(), key=Card.val)
        self.round_winners = [player for player in self.players 
                              if self.played_cards[player].val() == winning_rank]
        for player in self.round_winners:
            player.points += self.top_card.val() / len(self.round_winners)
    
    def __str__(self):
        points = {player: player.points for player in self.players}
        return f"Top Card: {self.top_card}, Played Cards: {self.played_cards}, Points:{points}"

    def is_over(self):
        return len(self.diamonds) == 0
